import { PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { IsEmail } from 'class-validator';
import { Role } from 'src/enums/role.enum';
import { CreateUserDto } from './create-user.dto';

export class UpdateUserDto extends PartialType(CreateUserDto) {
  @ApiProperty({ required: false, nullable: true })
  name?: string | null;

  @IsEmail()
  @ApiProperty({ required: false })
  email?: string;

  @ApiProperty({ required: false, nullable: true })
  roles?: Role[]

  @ApiProperty({ required: false, nullable: true })
  createdAt?: Date;

  @ApiProperty({ required: false, nullable: true })
  updatedAt?: Date;

  @ApiProperty({ required: false, nullable: true })
  experience?: number;

  @ApiProperty({ required: false, nullable: true })
  baseDailyRate?: number;

  @ApiProperty({ required: false, nullable: true })
  pictureURL?: string;
}
