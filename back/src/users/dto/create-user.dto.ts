import { IsEmail, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Role } from 'src/enums/role.enum';

export class CreateUserDto {
  @ApiProperty({ required: false, nullable: true })
  name?: string | null;

  @IsEmail()
  @ApiProperty()
  email: string;

  @IsNotEmpty()
  @ApiProperty()
  password: string;

  @ApiProperty({ required: false, nullable: true })
  roles?: Role[]

  @ApiProperty({ required: false, nullable: true })
  createdAt?: Date;

  @ApiProperty({ required: false, nullable: true })
  updatedAt?: Date;

  @ApiProperty({ required: false, nullable: true })
  experience?: number;

  @ApiProperty({ required: false, nullable: true })
  baseDailyRate?: number;

  @ApiProperty({ required: false, nullable: true })
  pictureURL?: string;
}
