import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
    CreateDateColumn,
    BeforeInsert,
    BaseEntity,
} from 'typeorm';
import * as bcrypt from 'bcryptjs';
import {Role} from 'src/enums/role.enum';
import { ApiProperty } from '@nestjs/swagger';

@Entity()
export class User extends BaseEntity {
    @PrimaryGeneratedColumn('uuid')
    @ApiProperty()
    id: number;

    @Column({length: 500, nullable: true})
    @ApiProperty()
    name: string;

    @Column({unique: true})
    @ApiProperty()
    email: string;

    @Column()
    @ApiProperty()
    password: string;

    @Column({
        type: "enum",
        enum: Role,
        default: [Role.User],
        array: true
    })
    @ApiProperty({ required: false, nullable: true })
    roles: Role[];

    @Column()
    @CreateDateColumn()
    @ApiProperty({ required: false, nullable: true })
    createdAt: Date;

    @Column()
    @UpdateDateColumn({nullable: true})
    @ApiProperty({ required: false, nullable: true })
    updatedAt: Date;

    @Column({type: 'int', nullable: true})
    @ApiProperty({ required: false, nullable: true })
    experience: number;

    @Column({type: 'int', nullable: true})
    @ApiProperty({ required: false, nullable: true })
    baseDailyRate: number;

    @Column({nullable: true})
    @ApiProperty({ required: false, nullable: true })
    pictureURL: string;

    @BeforeInsert()
    async hashPassword() {
        this.password = await bcrypt.hash(this.password, 8);
    }

    async validatePassword(password: string): Promise<boolean> {
        return bcrypt.compare(password, this.password);
    }
}
