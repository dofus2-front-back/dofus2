import {BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { ApiProperty } from '@nestjs/swagger';

@Entity()
export class Specialty extends BaseEntity {
    @PrimaryGeneratedColumn()
    @ApiProperty()
    id: number;

    @Column({length: 500})
    @ApiProperty()
    name: string;

    @Column()
    @ApiProperty({ required: false, nullable: true })
    description: string;
}
