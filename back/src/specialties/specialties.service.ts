import { Injectable } from '@nestjs/common';
import { CreateSpecialtyDto } from './dto/create-specialty.dto';
import { UpdateSpecialtyDto } from './dto/update-specialty.dto';
import { Specialty } from './entities/specialty.entity';

@Injectable()
export class SpecialtiesService {
  async create(createSpecialtyDto: CreateSpecialtyDto) {
    const specialty = Specialty.create(createSpecialtyDto);
    await specialty.save();

    return specialty;
  }

  async findAll(params: any) {
    return Specialty.find(params);
  }

  async findById(id: number) {
    return await Specialty.findOne(id);
  }

  async update(id: number, updateSpecialtyDto: UpdateSpecialtyDto) {
    return await Specialty.update(id, updateSpecialtyDto);
  }

  async remove(id: number) {
    return await Specialty.delete(id);
  }
}
