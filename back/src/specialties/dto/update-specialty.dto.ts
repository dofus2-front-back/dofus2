import { PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { CreateSpecialtyDto } from './create-specialty.dto';

export class UpdateSpecialtyDto extends PartialType(CreateSpecialtyDto) {
    @ApiProperty({ required: false })
    name?: string;
  
    @ApiProperty({ required: false, nullable: true })
    description?: string | null
}
