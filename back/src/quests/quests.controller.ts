import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Put,
} from '@nestjs/common';
import { QuestsService } from './quests.service';
import { CreateQuestDto } from './dto/create-quest.dto';
import { UpdateQuestDto } from './dto/update-quest.dto';
import { Adventurer } from 'src/adventurer/entities/adventurer.entity';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AddAdventurerQuestDto } from './dto/add-adventurer-quest.dto';

@ApiBearerAuth()
@Controller('quests')
@ApiTags('quests')
export class QuestsController {
  constructor(private readonly questsService: QuestsService) {}

  @Post()
  create(@Body() createQuestDto: CreateQuestDto) {
    return this.questsService.create(createQuestDto);
  }

  @Get()
  findAll() {
    return this.questsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.questsService.findById(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateQuestDto: UpdateQuestDto) {
    return this.questsService.update(+id, updateQuestDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.questsService.remove(+id);
  }

  @Patch('addAdventurer/:id')
  addAdventurer(@Param('id') id: string, @Body() addAdventurerQuestDto: AddAdventurerQuestDto) {
    return this.questsService.addAdventurer(+id, addAdventurerQuestDto);
  }

  @Patch('deleteAdventurer/:id')
  deleteAdventurer(@Param('id') id: string, @Body() addAdventurerQuestDto: AddAdventurerQuestDto) {
    return this.questsService.deleteAdventurer(+id, addAdventurerQuestDto);
  }
}
