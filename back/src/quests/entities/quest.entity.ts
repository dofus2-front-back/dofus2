import { ApiProperty } from "@nestjs/swagger";
import { Adventurer } from "src/adventurer/entities/adventurer.entity";
import { Request } from "src/request/entities/request.entity";
import { BaseEntity, Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Quest extends BaseEntity {
    @PrimaryGeneratedColumn()
    @ApiProperty()
    id: number;

    @OneToOne(() => Request)
    @JoinColumn()
    @ApiProperty()
    request: Request;

    @OneToMany(() => Adventurer, (adventurer: Adventurer) => adventurer.quest, { nullable: true, cascade: true })
    @ApiProperty({ required: false })
    group: Adventurer[];

  // @Column()
  // paiments: table relationnelle ?
}
