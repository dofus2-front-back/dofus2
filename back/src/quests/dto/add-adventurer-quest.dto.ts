import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class AddAdventurerQuestDto {
  @IsNotEmpty()
  @ApiProperty()
  id: number;
}
