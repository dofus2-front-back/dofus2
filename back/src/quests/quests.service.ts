import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Adventurer } from 'src/adventurer/entities/adventurer.entity';
import { getConnection, Repository } from 'typeorm';
import { AddAdventurerQuestDto } from './dto/add-adventurer-quest.dto';
import { CreateQuestDto } from './dto/create-quest.dto';
import { UpdateQuestDto } from './dto/update-quest.dto';
import { Quest } from './entities/quest.entity';

@Injectable()
export class QuestsService {
  constructor(
    @InjectRepository(Quest) private readonly repo: Repository<Quest>,
  ) {}

  async create(createQuestDto: CreateQuestDto) {
    const quest = Quest.create(createQuestDto);
    await quest.save();

    return quest;
  }

  findAll() {
    return Quest.find({ relations: ['group'] });
  }

  async findById(id: number) {
    const quest = await Quest.findOne(id, { relations: ['group'] });
    if (quest) {
      return quest;
    }
    throw new HttpException(
      {
        status: HttpStatus.FORBIDDEN,
        error: 'Aucune quest correspondant à cet id',
      },
      HttpStatus.FORBIDDEN,
    );
  }

  async update(id: number, updateQuestDto: UpdateQuestDto) {
    await Quest.update(id, updateQuestDto);
    const updateQuest = await Quest.findOne(id, { relations: ['group'] });
    if (updateQuest) {
      return updateQuest;
    }
    throw new HttpException(
      {
        status: HttpStatus.FORBIDDEN,
        error: 'impossible de mettre à jour la quest',
      },
      HttpStatus.FORBIDDEN,
    );
  }

  async remove(id: number) {
    return await Quest.delete(id);
  }

  async addAdventurer(id: number, data) {
    const quest = await Quest.findOne(id, { relations: ['group'] });
    quest.group.push(data);
    await Quest.save(quest);
  }

  async deleteAdventurer(id: number, addAdventurerQuestDto: AddAdventurerQuestDto) {
    const quest = await Quest.findOne(id, { relations: ["group"] })
    const newGroup = quest.group.filter(ad => ad.id !== addAdventurerQuestDto.id)
    quest.group = newGroup
    await Quest.save(quest)
  }
}
