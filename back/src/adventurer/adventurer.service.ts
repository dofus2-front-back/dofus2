import { Injectable } from '@nestjs/common';
import { CreateAdventurerDto } from './dto/create-adventurer.dto';
import { UpdateAdventurerDto } from './dto/update-adventurer.dto';
import { MoreThanOrEqual, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Adventurer } from './entities/adventurer.entity';

@Injectable()
export class AdventurerService {
  constructor(
    @InjectRepository(Adventurer) private readonly repo: Repository<Adventurer>,
  ) {}

  async create(createAdventurerDto: CreateAdventurerDto) {
    const adventurer = Adventurer.create(createAdventurerDto);
    await adventurer.save();

    return adventurer;
  }

  async findAll() {
    return Adventurer.find();
  }

  async findByFilters(params: any) {
    if (!params.experience && !params.speciality) {
      return await Adventurer.find();
    } else if (params.experience && params.speciality) {
      return await Adventurer.find({
        where: {
          experience: MoreThanOrEqual(params.experience),
          speciality: params.speciality,
        },
      });
    } else if (params.experience) {
      return await Adventurer.find({
        where: {
          experience: MoreThanOrEqual(params.experience),
        },
      });
    }
    return await Adventurer.find(params);
  }

  async findById(id: number) {
    return await Adventurer.findOne(id);
  }

  update(id: number, updateAdventurerDto: UpdateAdventurerDto) {
    return `This action updates a #${id} adventurer`;
  }

  remove(id: number) {
    return `This action removes a #${id} adventurer`;
  }
}
