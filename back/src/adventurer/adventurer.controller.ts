import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Req,
  Query,
} from '@nestjs/common';
import { find } from 'rxjs';
import { AdventurerService } from './adventurer.service';
import { CreateAdventurerDto } from './dto/create-adventurer.dto';
import { UpdateAdventurerDto } from './dto/update-adventurer.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

@ApiBearerAuth()
@Controller('adventurer')
@ApiTags('adventurer')
export class AdventurerController {
  constructor(private readonly adventurerService: AdventurerService) {}

  @Post()
  create(@Body() createAdventurerDto: CreateAdventurerDto) {
    return this.adventurerService.create(createAdventurerDto);
  }

  @Get()
  findAll(@Query() params) {
    return this.adventurerService.findByFilters(params);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.adventurerService.findById(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateAdventurerDto: UpdateAdventurerDto,
  ) {
    return this.adventurerService.update(+id, updateAdventurerDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.adventurerService.remove(+id);
  }
}
