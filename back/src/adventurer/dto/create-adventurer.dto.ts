import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CreateAdventurerDto {

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  name: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  speciality: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  pictureURL: string;

  @IsNumber()
  @IsNotEmpty()
  @ApiProperty()
  experience: number;

  @IsNumber()
  @IsNotEmpty()
  @ApiProperty()
  baseDailyRate: number;

}