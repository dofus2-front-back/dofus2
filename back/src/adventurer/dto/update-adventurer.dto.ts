import { PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { CreateAdventurerDto } from './create-adventurer.dto';

export class UpdateAdventurerDto extends PartialType(CreateAdventurerDto) {

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ required: false })
  name?: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ required: false })
  speciality?: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ required: false })
  pictureURL?: string;

  @IsNumber()
  @IsNotEmpty()
  @ApiProperty({ required: false })
  experience?: number;

  @IsNumber()
  @IsNotEmpty()
  @ApiProperty({ required: false })
  baseDailyRate?: number;

}
