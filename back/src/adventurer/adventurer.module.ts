import { Module } from '@nestjs/common';
import { AdventurerService } from './adventurer.service';
import { AdventurerController } from './adventurer.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Adventurer } from './entities/adventurer.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Adventurer])],
  controllers: [AdventurerController],
  providers: [AdventurerService],
  exports: [AdventurerService],
})
export class AdventurerModule {}
