import { ApiProperty } from '@nestjs/swagger';
import { Quest } from 'src/quests/entities/quest.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  ManyToOne,
  OneToMany,
} from 'typeorm';

@Entity()
export class Adventurer extends BaseEntity {
  @PrimaryGeneratedColumn()
  @ApiProperty()
  id: number;

  @Column({ length: 500 })
  @ApiProperty()
  name: string;

  @Column()
  @ApiProperty()
  speciality: string;

  @Column('int')
  @ApiProperty()
  experience: number;

  @Column('int')
  @ApiProperty()
  baseDailyRate: number;

  @Column()
  @ApiProperty()
  pictureURL: string;

  @ManyToOne(() => Quest, (quest: Quest) => quest.group, {
    nullable: true,
    onDelete: 'SET NULL',
  })
  quest: Quest;

  // @Column()
  // paiments: table relationnelle ?
}
