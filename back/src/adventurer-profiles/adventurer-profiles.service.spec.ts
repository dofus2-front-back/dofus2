import { Test, TestingModule } from '@nestjs/testing';
import { AdventurerProfilesService } from './adventurer-profiles.service';

describe('AdventurerProfilesService', () => {
  let service: AdventurerProfilesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AdventurerProfilesService],
    }).compile();

    service = module.get<AdventurerProfilesService>(AdventurerProfilesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
