import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { AdventurerProfilesService } from './adventurer-profiles.service';
import { CreateAdventurerProfileDto } from './dto/create-adventurer-profile.dto';
import { UpdateAdventurerProfileDto } from './dto/update-adventurer-profile.dto';

@Controller('adventurer-profiles')
export class AdventurerProfilesController {
  constructor(
    private readonly adventurerProfilesService: AdventurerProfilesService,
  ) {}

  @Post()
  create(@Body() createAdventurerProfileDto: CreateAdventurerProfileDto) {
    return this.adventurerProfilesService.create(createAdventurerProfileDto);
  }

  @Get()
  findAll() {
    return this.adventurerProfilesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.adventurerProfilesService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateAdventurerProfileDto: UpdateAdventurerProfileDto,
  ) {
    return this.adventurerProfilesService.update(
      +id,
      updateAdventurerProfileDto,
    );
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.adventurerProfilesService.remove(+id);
  }
}
