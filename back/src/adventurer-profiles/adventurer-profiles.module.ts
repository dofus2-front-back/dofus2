import { Module } from '@nestjs/common';
import { AdventurerProfilesService } from './adventurer-profiles.service';
import { AdventurerProfilesController } from './adventurer-profiles.controller';

@Module({
  controllers: [AdventurerProfilesController],
  providers: [AdventurerProfilesService]
})
export class AdventurerProfilesModule {}
