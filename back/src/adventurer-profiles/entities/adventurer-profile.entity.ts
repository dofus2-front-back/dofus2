import {
  BaseEntity,
  Column,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Request } from '../../request/entities/request.entity';

@Entity()
export class AdventurerProfile extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  speciality: string;

  @Column('int')
  experience: number;

  @ManyToOne(() => Request, (request: Request) => request.requiredProfiles, {
    nullable: true,
    onDelete: 'SET NULL',
  })
  request: Request;
}
