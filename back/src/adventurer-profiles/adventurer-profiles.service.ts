import { Injectable } from '@nestjs/common';
import { CreateAdventurerProfileDto } from './dto/create-adventurer-profile.dto';
import { UpdateAdventurerProfileDto } from './dto/update-adventurer-profile.dto';
import { AdventurerProfile } from './entities/adventurer-profile.entity';

@Injectable()
export class AdventurerProfilesService {
  async create(createAdventurerProfileDto: CreateAdventurerProfileDto) {
    const adventurer = AdventurerProfile.create(createAdventurerProfileDto);
    await adventurer.save();

    return adventurer;
  }

  findAll() {
    return AdventurerProfile.find();
  }

  async findOne(id: number) {
    return await AdventurerProfile.findOne(id);
  }

  update(id: number, updateAdventurerProfileDto: UpdateAdventurerProfileDto) {
    return `This action updates a #${id} adventurerProfile`;
  }

  remove(id: number) {
    return `This action removes a #${id} adventurerProfile`;
  }
}
