import { Test, TestingModule } from '@nestjs/testing';
import { AdventurerProfilesController } from './adventurer-profiles.controller';
import { AdventurerProfilesService } from './adventurer-profiles.service';

describe('AdventurerProfilesController', () => {
  let controller: AdventurerProfilesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AdventurerProfilesController],
      providers: [AdventurerProfilesService],
    }).compile();

    controller = module.get<AdventurerProfilesController>(AdventurerProfilesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
