import { PartialType } from '@nestjs/mapped-types';
import { CreateAdventurerProfileDto } from './create-adventurer-profile.dto';

export class UpdateAdventurerProfileDto extends PartialType(CreateAdventurerProfileDto) {}
