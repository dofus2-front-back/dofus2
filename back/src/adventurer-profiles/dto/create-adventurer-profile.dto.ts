import {IsNotEmpty, IsNumber, IsString} from "class-validator";

export class CreateAdventurerProfileDto {
    @IsString()
    @IsNotEmpty()
    speciality: string;

    @IsNumber()
    @IsNotEmpty()
    experience: number;
}
