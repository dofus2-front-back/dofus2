import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { AdventurerProfile } from '../../adventurer-profiles/entities/adventurer-profile.entity';
import { ApiProperty } from "@nestjs/swagger";

export enum QuestStatus {
  UNASSIGNED = 'Unassigned',
  PENDING = 'Pending',
  FAILED = 'Failed',
  SUCCEEDED = 'Succeeded',
  REJECTED = 'Rejected',
}

@Entity()
export class Request extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({length: 500})
    @ApiProperty()
    name: string;

    @Column()
    @ApiProperty()
    description: string;

    @Column({length: 500})
    @ApiProperty()
    pictureURL: string;

    @Column({length: 500})
    @ApiProperty()
    questGiver: string;

    @Column()
    @ApiProperty()
    bounty: number;

    @Column()
    @ApiProperty()
    duration: number;

    @Column()
    @ApiProperty()
    awardedExperience: number;

    @Column({
        type: "enum",
        enum: QuestStatus,
        default: QuestStatus.UNASSIGNED
    })
    @ApiProperty({ required: false, nullable: true })
    status: QuestStatus;

    @Column()
    @CreateDateColumn()
    @ApiProperty({ required: false, nullable: true })
    date: Date;

  @Column()
  @ApiProperty()
  availableFor: number;

  @OneToMany(
    () => AdventurerProfile,
    (adventurerProfile: AdventurerProfile) => adventurerProfile.request,
    {
      nullable: true,
      cascade: true,
    },
  )
  requiredProfiles: AdventurerProfile[];
}
