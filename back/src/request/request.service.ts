import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateRequestDto } from './dto/create-request.dto';
import { UpdateRequestDto } from './dto/update-request.dto';
import { Request } from './entities/request.entity';

@Injectable()
export class RequestService {
  async create(creatRequestDto: CreateRequestDto) {
    const request = Request.create(creatRequestDto);
    await request.save();
    return request;
  }

  async findAll(params: any) {
    return Request.find({
      where: params,
      relations: ['requiredProfiles'],
    });
  }

  async findById(id: number) {
    const request = await Request.findOne(id, {
      relations: ['requiredProfiles'],
    });
    if (request) {
      return request;
    }
    throw new HttpException(
      {
        status: HttpStatus.FORBIDDEN,
        error: 'Aucune request correspondant à cet id',
      },
      HttpStatus.FORBIDDEN,
    );
  }

  async update(id: number, updateRequestDto: UpdateRequestDto) {
    return await Request.update(id, updateRequestDto);
  }

  async remove(id: number) {
    return await Request.delete(id);
  }
}
