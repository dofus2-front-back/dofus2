import { PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsString } from 'class-validator';
import { QuestStatus } from '../entities/request.entity';
import { CreateRequestDto } from './create-request.dto';

export class UpdateRequestDto extends PartialType(CreateRequestDto) {
    @IsString()
    @ApiProperty({ required: false })
    name: string;

    @IsString()
    @ApiProperty({ required: false })
    description: string;

    @IsString()
    @ApiProperty({ required: false })
    pictureURL: string;

    @IsString()
    @ApiProperty({ required: false })
    questGiver: string;

    @IsNumber()
    @ApiProperty({ required: false })
    bounty: number;

    @IsNumber()
    @ApiProperty({ required: false })
    duration: number;

    @IsNumber()
    @ApiProperty({ required: false })
    awardedExperience: number;

    @IsString()
    @ApiProperty({ required: false })
    availableFor: number;

    @ApiProperty({ required: false, nullable: true })
    status?: QuestStatus;

    @ApiProperty({ required: false, nullable: true })
    date?: Date;
}
