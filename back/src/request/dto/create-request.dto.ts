import { ApiProperty } from "@nestjs/swagger";
import {IsDate, IsNotEmpty, IsNumber, IsString} from "class-validator";
import { QuestStatus } from "../entities/request.entity";

export class CreateRequestDto {

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    name: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    description: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    pictureURL: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    questGiver: string;

    @IsNumber()
    @IsNotEmpty()
    @ApiProperty()
    bounty: number;

    @IsNumber()
    @IsNotEmpty()
    @ApiProperty()
    duration: number;

    @IsNumber()
    @IsNotEmpty()
    @ApiProperty()
    awardedExperience: number;

    @IsNumber()
    @IsNotEmpty()
    @ApiProperty()
    availableFor: number;

    @ApiProperty({ required: false, nullable: true })
    status?: QuestStatus;

    @ApiProperty({ required: false, nullable: true })
    date?: Date;
}
