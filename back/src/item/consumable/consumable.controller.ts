import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ConsumableService } from './consumable.service';
import { CreateConsumableDto } from './dto/create-consumable.dto';
import { UpdateConsumableDto } from './dto/update-consumable.dto';

@Controller('consumable')
export class ConsumableController {
  constructor(private readonly consumableService: ConsumableService) {}

  @Post()
  create(@Body() createConsumableDto: CreateConsumableDto) {
    return this.consumableService.create(createConsumableDto);
  }

  @Get()
  findAll() {
    return this.consumableService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.consumableService.findById(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateConsumableDto: UpdateConsumableDto,
  ) {
    return this.consumableService.update(+id, updateConsumableDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.consumableService.remove(+id);
  }
}
