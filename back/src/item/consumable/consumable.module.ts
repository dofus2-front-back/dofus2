import { Module } from '@nestjs/common';
import { ConsumableService } from './consumable.service';
import { ConsumableController } from './consumable.controller';

@Module({
  controllers: [ConsumableController],
  providers: [ConsumableService]
})
export class ConsumableModule {}
