import { Injectable } from '@nestjs/common';
import { CreateConsumableDto } from './dto/create-consumable.dto';
import { UpdateConsumableDto } from './dto/update-consumable.dto';
import { Consumable } from './entities/consumable.entity';

@Injectable()
export class ConsumableService {
  async create(createConsumableDto: CreateConsumableDto) {
    const consumable = Consumable.create(createConsumableDto);
    await consumable.save();

    return consumable;
  }

  findAll() {
    return Consumable.find();
  }

  findById(id: number) {
    return Consumable.findOne(id);
  }

  update(id: number, updateConsumableDto: UpdateConsumableDto) {
    return Consumable.update(id, updateConsumableDto);
  }

  remove(id: number) {
    return Consumable.delete(id);
  }
}
