import { Item } from 'src/item/entities/item.entity';
import { Column, Entity } from 'typeorm';

@Entity()
export class Consumable extends Item {
  @Column('int')
  charges: number;

  @Column('int')
  usedCharges: number;
}
