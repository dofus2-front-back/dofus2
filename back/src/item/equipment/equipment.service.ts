import { Injectable } from '@nestjs/common';
import { CreateEquipmentDto } from './dto/create-equipment.dto';
import { UpdateEquipmentDto } from './dto/update-equipment.dto';
import { Equipment } from './entities/equipment.entity';

@Injectable()
export class EquipmentService {
  async create(createEquipmentDto: CreateEquipmentDto) {
    const equipment = Equipment.create(createEquipmentDto);
    await equipment.save();

    return equipment;
  }

  async findAll() {
    return Equipment.find();
  }

  async findById(id: number) {
    return await Equipment.findOne(id);
  }

  async findByFilters(params: any) {
    return await Equipment.find(params);
  }

  update(id: number, updateEquipmentDto: UpdateEquipmentDto) {
    return Equipment.update(id, updateEquipmentDto);
  }

  remove(id: number) {
    return Equipment.delete(id);
  }
}
