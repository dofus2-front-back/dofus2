import { Item } from 'src/item/entities/item.entity';
import { Entity, Column } from 'typeorm';

@Entity()
export class Equipment extends Item {
  @Column('int')
  durability: number;

  @Column('int')
  daysInUse: number;

  @Column('int')
  repairTime: number;
}
