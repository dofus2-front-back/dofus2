import { Module } from '@nestjs/common';
import { ItemService } from './item.service';
import { ItemController } from './item.controller';
import { EquipmentModule } from './equipment/equipment.module';
import { ConsumableModule } from './consumable/consumable.module';

@Module({
  controllers: [ItemController],
  providers: [ItemService],
  imports: [EquipmentModule, ConsumableModule],
})
export class ItemModule {}
