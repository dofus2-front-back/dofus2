import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {configService} from './config/config.service';
import {UsersModule} from './users/users.module';
import {RequestModule} from './request/request.module';
import {AuthModule} from './auth/auth.module';
import {AdventurerModule} from './adventurer/adventurer.module';
import {APP_GUARD} from '@nestjs/core';
import {RolesGuard} from './auth/roles.guard';
import {JwtAuthGuard} from './auth/jwt-auth.guard';
import { QuestsModule } from './quests/quests.module';
import { SpecialtiesModule } from './specialties/specialties.module';
import { AdventurerProfilesModule } from './adventurer-profiles/adventurer-profiles.module';

@Module({
    imports: [
        TypeOrmModule.forRoot(configService.getTypeOrmConfig()),
        UsersModule,
        RequestModule,
        AuthModule,
        QuestsModule,
        AdventurerModule,
        SpecialtiesModule,
        AdventurerProfilesModule,
    ],
    controllers: [AppController],
    providers: [AppService, {
        provide: APP_GUARD,
        useClass: JwtAuthGuard,
    }, {
        provide: APP_GUARD,
        useClass: RolesGuard,
    }],
})
export class AppModule {
}
