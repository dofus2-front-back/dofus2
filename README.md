# Dofus 2

[Production](https://dofus2.herokuapp.com/)

[Développement](https://dofus2-dev.herokuapp.com)


## Installation :
Cloner le projet :

https://gitlab.com/dofus2-front-back/dofus2.git

Se placer à la racine du projet

### Installer le front :
cd front/
npm install
ng serve
http://localhost:4200

### Installer le back :
Il faut créer à la racine un .env avec les informations suivantes :

`POSTGRES_HOST=localhost
POSTGRES_PORT=5433
POSTGRES_USER=postgres
POSTGRES_PASSWORD=password
POSTGRES_DATABASE=dofus2-db
PORT=3000
MODE=DEV
RUN_MIGRATIONS=true
JWT_SECRET=jfjdffdjhjkdfhdfjhdf`

Nous utilisons POSTGRESQL 13, la table est générée automatiquement et il faut créer une base de données et mettre le même nom dans le .env
cd back/
npm install
nest start
http://localhost:3000


