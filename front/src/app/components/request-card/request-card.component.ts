import { Component, OnInit, Input } from '@angular/core';
import { QuestStatus, Request } from 'src/app/models/request.model';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-request-card',
  templateUrl: './request-card.component.html',
})
export class RequestCardComponent implements OnInit {
  constructor() {}
  @Input() request: Request = {
    name: '',
    description: '',
    pictureURL: '',
    questGiver: '',
    bounty: 0,
    duration: 0,
    requiredProfiles: [],
    awardedExperience: 0,
    status: QuestStatus.Unassigned,
    date: new Date(),
    availableFor: 0,
  };

  getDifferenceBetweenDates(date: Date, availableFor: number) {
    const startDate = new Date(date);
    const endDate = new Date(
      startDate.setDate(startDate.getDate() + availableFor)
    );
    const today = new Date();
    const diff = (today.getTime() - endDate.getTime()) * -1;
    const diffDays = Math.ceil(diff / (1000 * 3600 * 24));
    if (Math.sign(diffDays) === -1 || diffDays === 0) {
      return 'Unavailable';
    } else {
      return 'Available for ' + diffDays + 'd';
    }
  }

  ngOnInit(): void {}
}
