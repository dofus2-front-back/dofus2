import { Component, Input, OnInit } from '@angular/core';
import { Adventurer } from 'src/app/models/adventurer.model';

@Component({
  selector: 'app-adventurer-card',
  templateUrl: './adventurer-card.component.html',
})
export class AdventurerCardComponent implements OnInit {
  constructor() {}

  @Input() adventurer: Adventurer = {
    name: '',
    speciality: '',
    experience: 0,
    baseDailyRate: 0,
    pictureURL: '',
  };

  ngOnInit(): void {}
}
