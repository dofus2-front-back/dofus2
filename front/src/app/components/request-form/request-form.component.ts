import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { RequestService } from '../../services/request.service';
import { RequestForm } from '../../models/request.model';
import { AdventurerProfile } from 'src/app/models/adventurer-profile.model';

@Component({
  selector: 'app-request-form',
  templateUrl: './request-form.component.html',
})
export class RequestFormComponent implements OnInit {
  requiredProfiles: Array<AdventurerProfile> = [];
  specialityList: Array<String> = ['Warrior', 'Wizard', 'Archer', 'Assassin'];

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private requestService: RequestService
  ) {}

  ngOnInit(): void {}

  requestForm = this.fb.group({
    name: ['', Validators.required],
    description: ['', Validators.required],
    pictureURL: [''],
    questGiver: ['', Validators.required],
    bounty: [0, [Validators.required, Validators.min(0)]],
    duration: [
      0,
      [Validators.required, Validators.min(0), Validators.max(365)],
    ],
    awardedExperience: [0, [Validators.required, Validators.min(0)]],
    availableFor: [
      0,
      [Validators.required, Validators.min(0), Validators.max(365)],
    ],
  });

  requiredProfilesForm = this.fb.group({
    speciality: ['', [Validators.required]],
    experience: [0, [Validators.required, Validators.min(0)]],
  });

  onSubmit() {
    this.requestService
      .createRequest(this.requestForm.value)
      .subscribe((res) => {
        this.router.navigate(['/request']);
      });
  }

  onAddProfile() {
    this.requiredProfiles = [
      ...this.requiredProfiles,
      this.requiredProfilesForm.value,
    ];
  }
}
