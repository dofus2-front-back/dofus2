import { Component, OnInit } from '@angular/core';
import { Adventurer } from 'src/app/models/adventurer.model';
import { AdventurerService } from 'src/app/services/adventurer.service';

@Component({
  selector: 'app-adventurer',
  templateUrl: './adventurer.component.html',
})
export class AdventurerComponent implements OnInit {
  constructor(private adventurerService: AdventurerService) {}

  ngOnInit(): void {
    this.getAllAdventurers();
  }

  public adventurers: Array<Adventurer> = [];
  public searchTerm: string = '';
  public searchedAdventurers: Array<Adventurer> = [];

  filter(ev: any) {
    this.filterItems(ev.target.value);
}
public filterItems(searchTerm: string) {
    this.searchedAdventurers = this.adventurers.filter((adventurers) => {
        if (searchTerm === '') {
            return this.adventurers;
        }
        return adventurers.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });
}

  public getAllAdventurers() {
    return this.adventurerService
      .getAllAdventurers()
      .subscribe((response: Array<Adventurer>) => {
        this.adventurers = response;
        this.searchedAdventurers = response;
      });
  }
}
