import { Component, OnInit } from '@angular/core';
import { QuestStatus, Request } from 'src/app/models/request.model';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
})
export class RequestComponent implements OnInit {
  constructor(private requestService: RequestService) {}

  ngOnInit(): void {
    this.getAllRequests();
  }

  public requests: Array<Request> = [];
  public searchTerm: string = '';
  public searchedRequests: Array<Request> = [];

  filter(ev: any) {
    this.filterItems(ev.target.value);
  }
public filterItems(searchTerm: string) {
    this.searchedRequests = this.requests.filter((requests) => {
        if (searchTerm === '') {
            return this.requests;
        }
        return requests.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });
}

  public getAllRequests() {
    return this.requestService
      .getAllRequests()
      .subscribe((response: Array<Request>) => {
        this.requests = response;
        this.searchedRequests = response;
      });
  }
}
