import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-authentification',
  templateUrl: './authentification.component.html',
})
export class AuthentificationComponent {
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthenticationService
  ) {}

  signinForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', Validators.required],
  });

  public onSignin() {
    this.authService.signin(this.signinForm.value).subscribe((data: any) => {
      localStorage.setItem('token', data.access_token);
      this.router.navigate(['request']);
    });
  }
}
