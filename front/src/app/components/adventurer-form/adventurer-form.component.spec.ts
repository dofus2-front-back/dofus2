import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { async, fakeAsync, TestBed } from '@angular/core/testing';
import { AdventurerService } from '../../services/adventurer.service';
import { environment } from '../../../environments/environment';

describe('Test creation adventurer', () => {
  let httpTestingController: HttpTestingController;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    }).compileComponents();
  }));

  let adventurerService: AdventurerService;
  beforeEach(() => (adventurerService = TestBed.get(AdventurerService)));

  beforeEach(
    () => (httpTestingController = TestBed.get(HttpTestingController))
  );

  afterEach(() => {
    httpTestingController.verify();
  });

  it('Should create adventurer', fakeAsync(() => {
    let adventurer: any;

    adventurerService
      .createAdventurer({
        name: 'Yasuo',
        speciality: 'Assassin',
        experience: 100,
        baseDailyRate: 50,
        pictureURL:
          'https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Yasuo_10.jpg',
      })
      .subscribe((_adventurer) => (adventurer = _adventurer));

    const req = httpTestingController.expectOne(
      `${environment.apiUrl}/adventurer`
    );

    req.flush({
      name: 'Yasuo',
      speciality: 'Assassin',
      experience: 100,
      baseDailyRate: 50,
      pictureURL:
        'https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Yasuo_10.jpg',
    });

    expect(adventurer.name).toEqual('Yasuo');
  }));
});
