import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { AdventurerService } from '../../services/adventurer.service';

@Component({
  selector: 'app-adventurer-form',
  templateUrl: './adventurer-form.component.html',
})
export class AdventurerFormComponent implements OnInit {
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private adventurerService: AdventurerService
  ) {}

  ngOnInit(): void {}

  adventurerForm = this.fb.group({
    name: ['', Validators.required],
    speciality: ['', Validators.required],
    experience: ['', [Validators.required, Validators.min(0)]],
    baseDailyRate: ['', [Validators.required, Validators.min(0)]],
    pictureURL: [''],
  });

  onSubmit() {
    this.adventurerService
      .createAdventurer(this.adventurerForm.value)
      .subscribe((res) => {
        console.log(res);
        this.router.navigate(['/adventurer']);
      });
  }
}
