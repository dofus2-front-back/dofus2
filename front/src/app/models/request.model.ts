import { AdventurerProfile } from './adventurer-profile.model';

export interface RequestForm {
  name: String;
  description: String;
  pictureURL: String;
  questGiver: String;
  bounty: Number;
  duration: Number;
  requiredProfiles: Array<AdventurerProfile>;
  awardedExperience: Number;
  availableFor: number;
}

export interface Request extends RequestForm {
  status: QuestStatus;
  date: Date;
}

export enum QuestStatus {
  Unassigned = 'Unassigned',
  Pending = 'Pending',
  Failed = 'Failed',
  Succeeded = 'Succeeded',
  Rejected = 'Rejected',
}
