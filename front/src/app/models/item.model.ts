import { Transaction } from './transaction.model';

export interface Item {
  name: String;
  price: Number;
  transaction: Transaction;
}

export interface Equipment extends Item {
  durability: Number;
  daysInUse: Number;
  repairTime: Number;
}

export interface Consumable extends Item {
  charges: Number;
  usedCharges: Number;
}
