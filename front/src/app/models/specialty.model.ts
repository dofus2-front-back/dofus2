import { Item } from './item.model';

export interface Specialty {
  name: string;
  description?: string;
  requiredItems?: Array<Item>;
}
