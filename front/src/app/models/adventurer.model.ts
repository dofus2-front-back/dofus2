import { Specialty } from './specialty.model';

export interface Adventurer {
  name: String;
  // @TODO: Replace String to Specialty for specialty element
  speciality: String;
  experience: Number;
  baseDailyRate: Number;
  pictureURL: String;
}
