import { Adventurer } from './adventurer.model';
import { Transaction } from './transaction.model';

export interface Quest {
  request: Request;
  group: Array<Adventurer>;
  payment: Transaction;
}
