export interface Transaction {
  amount: Number;
  type: TransactionType;
  date: Date;
}

export enum TransactionType {
  QuestBounty = 'QuestBounty',
  AdventurerPayment = 'AdventurerPayment',
  Purchase = 'Purchase',
  Tax = 'Tax',
}
