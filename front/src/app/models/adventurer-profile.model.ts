import { Specialty } from './specialty.model';

export interface AdventurerProfile {
  // @TODO: Replace String to Specialty for specialty element
  speciality: String;
  experience: Number;
}
