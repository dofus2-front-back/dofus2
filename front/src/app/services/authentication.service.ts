import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { SignIn } from '../models/signin.model';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  constructor(private http: HttpClient) {}

  public signin(signin: SignIn) {
    return this.http.post(`${environment.apiUrl}/auth/login`, signin);
  }
}
