import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Adventurer } from '../models/adventurer.model';

@Injectable({
  providedIn: 'root',
})
export class AdventurerService {
  constructor(private http: HttpClient) {}

  public getAllAdventurers() {
    return this.http.get<Array<Adventurer>>(
      `${environment.apiUrl}/adventurer`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      }
    );
  }

  public createAdventurer(adventurer: {
    speciality: string;
    pictureURL: string;
    name: string;
    experience: number;
    baseDailyRate: number;
  }) {
    return this.http.post(`${environment.apiUrl}/adventurer`, adventurer, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    });
  }
}
