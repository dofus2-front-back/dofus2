import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthentificationComponent } from './components/authentification/authentification.component';
import { RequestComponent } from './components/request/request.component';
import { RequestFormComponent } from './components/request-form/request-form.component';
import { AdventurerComponent } from './components/adventurer/adventurer.component';
import { AdventurerFormComponent } from './components/adventurer-form/adventurer-form.component';
import { AuthGuard } from './services/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: RequestComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'login',
    component: AuthentificationComponent,
  },
  {
    path: 'request',
    component: RequestComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'request/new',
    component: RequestFormComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'adventurer',
    component: AdventurerComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'adventurer/new',
    component: AdventurerFormComponent,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
