import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthentificationComponent } from './components/authentification/authentification.component';
import { DefaultLayoutComponent } from './layouts/default-layout/default-layout.component';
import { AuthenticationService } from './services/authentication.service';
import { RequestComponent } from './components/request/request.component';
import { RequestCardComponent } from './components/request-card/request-card.component';
import { RequestFormComponent } from './components/request-form/request-form.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { AdventurerFormComponent } from './components/adventurer-form/adventurer-form.component';
import { AdventurerComponent } from './components/adventurer/adventurer.component';
import { RequestService } from './services/request.service';
import { AdventurerService } from './services/adventurer.service';
import { AdventurerCardComponent } from './components/adventurer-card/adventurer-card.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthentificationComponent,
    DefaultLayoutComponent,
    RequestComponent,
    RequestCardComponent,
    RequestFormComponent,
    NavbarComponent,
    AdventurerFormComponent,
    AdventurerComponent,
    AdventurerCardComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
  ],
  providers: [AuthenticationService, RequestService, AdventurerService],
  bootstrap: [AppComponent],
})
export class AppModule {}
