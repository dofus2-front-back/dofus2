FROM alpine:latest
RUN apk add --update --no-cache nginx nodejs supervisor
RUN mkdir -p /run/nginx && ln -sf /dev/stdout /var/log/nginx/access.log && ln -sf /dev/stderr /var/log/nginx/error.log
RUN set -x \
        && rm -f /etc/nginx/nginx.conf \
        && rm -f /etc/nginx/conf.d/default.conf
COPY config/nginx.prod.conf /etc/nginx/nginx.conf
COPY config/supervisord.ini /etc/supervisor.d/supervisor.ini
COPY front/dist/app/ /usr/share/nginx/html/
COPY back/dist/ /opt/app/dist/
COPY back/node_modules/ /opt/app/node_modules/
ENV LISTEN_PORT 8080
EXPOSE 8080
RUN adduser -D myuser
USER myuser
CMD sed -i -e 's/{PORT}/'"$PORT"'/g' /etc/nginx/nginx.conf && supervisord --nodaemon --configuration /etc/supervisord.conf
